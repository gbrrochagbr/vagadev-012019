var cart = 0;

window.onload = () => {
  setCart();
};

toggleMenu = () => {
  document
    .querySelector(".menuMobileList")
    .classList.toggle("menuMobileList--active");
  document.querySelector(".backBlock").classList.toggle("backBlock--active");
};

buy = () => {
  cart++;
  setCart();
  document.querySelector(".modalVenda").classList.toggle("modalVenda--active");
  document.querySelector("#backBlock").classList.toggle("backBlock--active");
};

closeModal = () => {
  document.querySelector(".modalVenda").classList.toggle("modalVenda--active");
  document.querySelector("#backBlock").classList.toggle("backBlock--active");
};

setCart = () => {
  document.querySelector(".circleCart").textContent = cart;
};
