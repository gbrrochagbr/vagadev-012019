//Variables
var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var clean = require('gulp-clean');



//Sass source
var scssFiles = './styles/*.scss';
//Css Destination
var buildDest = './build';
//Option for development
var sassDevOptions = {
    outputStyle : 'expanded'
}
//Option for Production
var sassProdOptions = {
    outputStyle : 'compressed'
}

//Tasks
gulp.task('clean', function(){
    return gulp.src(buildDest, {read:false})
    .pipe(clean());
});
gulp.task('sassdev', function(){
    return gulp.src(scssFiles)
    .pipe(sass(sassDevOptions).on('error', sass.logError))
    .pipe(rename('main.css'))
    .pipe(gulp.dest(buildDest));
});
gulp.task('sassprod', function(){
    return gulp.src(scssFiles)
    .pipe(sass(sassProdOptions).on('error', sass.logError))
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest(buildDest));
});
gulp.task('watch', function(){
    gulp.watch(scssFiles, ['sassdev', 'sassprod']);
});

gulp.task('default', ['clean','sassdev', 'sassprod','watch']);